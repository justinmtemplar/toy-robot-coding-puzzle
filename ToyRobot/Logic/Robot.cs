﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// Solution created by Justin Templar, 2020

namespace ToyRobot.Logic
{
    public class Robot
    {
        public Table Table { get; set; }

        public TablePoint Point { get; set; }

        public bool HasAPoint => Point is null == false;

        public Robot(Table table)
        {
            if (table is null)
                throw new NullReferenceException("Table Top cannot be null!");

            this.Table = table;
        }

        public bool Place(TablePoint point)
        {
            if (!Table.IsPointValid(point))
                return false;

            this.Point = point;
            // Only add Robot to table if valid
            Table.Robots.Add(this);
            return true;
        }

        public void TurnLeft()
        {
            if (Table == null)
                throw new NullReferenceException("Table Top cannot be null!");

            if (!HasAPoint)
                return;

            // turning won't cause any issues...
            switch (Point.Direction)
            {
                case Direction.North: Point.Direction = Direction.West; break;
                case Direction.East: Point.Direction = Direction.North; break;
                case Direction.South: Point.Direction = Direction.East; break;
                case Direction.West: Point.Direction = Direction.South; break;
            }
        }

        public void TurnRight()
        {
            if (Table == null)
                throw new NullReferenceException("Table Top cannot be null!");

            if (!HasAPoint)
                return;

            // turning won't cause any issues...
            switch (Point.Direction)
            {
                case Direction.North: Point.Direction = Direction.East; break;
                case Direction.East: Point.Direction = Direction.South; break;
                case Direction.South: Point.Direction = Direction.West; break;
                case Direction.West: Point.Direction = Direction.North; break;
            }
        }

        public bool Move()
        {
            if (Table == null)
                throw new NullReferenceException("Board cannot be null!");

            if (!HasAPoint)
                return false;

            // calculate the desired board position
            var newX = Point.Direction == Direction.East
                                // robot facing east, increment x
                                ? Point.X + 1
                                : Point.Direction == Direction.West
                                    // robot facing west, decrement x
                                    ? Point.X - 1
                                    // robot facing neither east or west, so don't change x.
                                    : Point.X;

            var newY = Point.Direction == Direction.North
                                // robot facing north, increment y
                                ? Point.Y + 1
                                : Point.Direction == Direction.South
                                    // robot facing south, decrement y
                                    ? Point.Y - 1
                                    // robot facing neither north or south, so don't change Y.
                                    : Point.Y;

            var newPoint = new TablePoint(newX, newY, Point.Direction);

            if (!Table.IsPointValid(newPoint))
                return false;

            // update position
            Point = newPoint;
            return true;
        }

        public string ExecuteCommand(RobotCommand command)
        {
            // This 

            if (!HasAPoint && command.Command != Command.Place)
                return "You must place your Robot before other commands.";

            switch (command.Command)
            {
                case Command.Place:
                    if (command.TablePoint == null)
                        return "Place command missing position information.";
                    if (!this.Place(command.TablePoint))
                            return "The Robot placement is not on the table.";
                    return String.Format("Your Robot is now placed at X:{0} Y:{1} Facing:{2}", this.Point.X, this.Point.Y, this.Point.Direction);
                case Command.Move:
                    if (!this.Move())
                        return "There was no room to move your Robot.";
                    return "Your Robot has moved forward.";
                case Command.Left:
                    this.TurnLeft();
                    return "Your Robot has turned left.";
                case Command.Right:
                    this.TurnRight();
                    return "Your Robot has turned right.";
                case Command.Report:
                    return String.Format("Your Robot's current position is X:{0} Y:{1} Facing:{2}", this.Point.X, this.Point.Y, this.Point.Direction);
            }
            return "Unknown command.";
        }
    }
}
