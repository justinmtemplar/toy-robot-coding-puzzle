﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// Solution created by Justin Templar, 2020

namespace ToyRobot.Logic
{
    public enum Command
    {
        Place,
        Move,
        Left,
        Right,
        Report
    }

    public class RobotCommand
    {
        public Command Command { get; set; }
        public TablePoint TablePoint { get; set; }

        public RobotCommand(Command command)
        {
            this.Command = command;
            this.TablePoint = null;
        }

        public RobotCommand(Command command, TablePoint tablePoint)
        {
            this.Command = command;
            this.TablePoint = tablePoint;
        }
    }
}
