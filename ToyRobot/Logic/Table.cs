﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// Solution created by Justin Templar, 2020

namespace ToyRobot.Logic
{
    public class Table
    {
        public int Size { get; private set; } = 5;
        public IList<Robot> Robots { get; set; }

        public Table()
        {
            this.Robots = new List<Robot>();
        }

        public Table(int size)
        {
            this.Size = size;
            this.Robots = new List<Robot>();
        }

        public bool IsPointValid(TablePoint point)
        {
            return point.X >= 0 && point.X < Size && point.Y >= 0 && point.Y < Size;
        }

        public bool IsPointOccupied(TablePoint point)
        {
            return Robots.Any(x => x.Point.Equals(point));
        }

        public Robot GetRobotAtPosition(TablePoint point)
        {
            return Robots.FirstOrDefault(x => x.Point.Equals(point));
        }
    }
}
