﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

// Solution created by Justin Templar, 2020

namespace ToyRobot.Logic
{
    public enum Direction
    {
        North,
        South,
        East,
        West
    };

    public class TablePoint
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Direction Direction { get; set; }

        public TablePoint(int x, int y, Direction direction)
        {
            this.X = x;
            this.Y = y;
            this.Direction = direction;
        }

        public override bool Equals(Object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                TablePoint p = (TablePoint)obj;
                return (X == p.X) && (Y == p.Y);
            }
        }

        public override int GetHashCode()
        {
            return (X << 2) ^ Y;
        }

        public override string ToString()
        {
            return String.Format("GridPoint({0}, {1}, {2})", X, Y, this.Direction);
        }
    }
}
